require 'test_helper'

class IpsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ip = ips(:one)
  end

  test "should get index" do
    get ips_url
    assert_response :success
  end

  test "should get new" do
    get new_ip_url
    assert_response :success
  end

  test "should create ip" do
    assert_difference('Ip.count') do
      post ips_url, params: { ip: { city: @ip.city, country: @ip.country, hostname: @ip.hostname, ip_address: @ip.ip_address, is_bot: @ip.is_bot, is_exploit_bot: @ip.is_exploit_bot, is_hijacked: @ip.is_hijacked, is_listed: @ip.is_listed, is_malware: @ip.is_malware, is_spam_bot: @ip.is_spam_bot, is_spyware: @ip.is_spyware, last_checked_at: @ip.last_checked_at, region: @ip.region } }
    end

    assert_redirected_to ip_url(Ip.last)
  end

  test "should show ip" do
    get ip_url(@ip)
    assert_response :success
  end

  test "should get edit" do
    get edit_ip_url(@ip)
    assert_response :success
  end

  test "should update ip" do
    patch ip_url(@ip), params: { ip: { city: @ip.city, country: @ip.country, hostname: @ip.hostname, ip_address: @ip.ip_address, is_bot: @ip.is_bot, is_exploit_bot: @ip.is_exploit_bot, is_hijacked: @ip.is_hijacked, is_listed: @ip.is_listed, is_malware: @ip.is_malware, is_spam_bot: @ip.is_spam_bot, is_spyware: @ip.is_spyware, last_checked_at: @ip.last_checked_at, region: @ip.region } }
    assert_redirected_to ip_url(@ip)
  end

  test "should destroy ip" do
    assert_difference('Ip.count', -1) do
      delete ip_url(@ip)
    end

    assert_redirected_to ips_url
  end
end
