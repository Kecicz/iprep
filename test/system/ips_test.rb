require "application_system_test_case"

class IpsTest < ApplicationSystemTestCase
  setup do
    @ip = ips(:one)
  end

  test "visiting the index" do
    visit ips_url
    assert_selector "h1", text: "Ips"
  end

  test "creating a Ip" do
    visit ips_url
    click_on "New Ip"

    fill_in "City", with: @ip.city
    fill_in "Country", with: @ip.country
    fill_in "Hostname", with: @ip.hostname
    fill_in "Ip address", with: @ip.ip_address
    fill_in "Is bot", with: @ip.is_bot
    fill_in "Is exploit bot", with: @ip.is_exploit_bot
    fill_in "Is hijacked", with: @ip.is_hijacked
    fill_in "Is listed", with: @ip.is_listed
    fill_in "Is malware", with: @ip.is_malware
    fill_in "Is spam bot", with: @ip.is_spam_bot
    fill_in "Is spyware", with: @ip.is_spyware
    fill_in "Last checked at", with: @ip.last_checked_at
    fill_in "Region", with: @ip.region
    click_on "Create Ip"

    assert_text "Ip was successfully created"
    click_on "Back"
  end

  test "updating a Ip" do
    visit ips_url
    click_on "Edit", match: :first

    fill_in "City", with: @ip.city
    fill_in "Country", with: @ip.country
    fill_in "Hostname", with: @ip.hostname
    fill_in "Ip address", with: @ip.ip_address
    fill_in "Is bot", with: @ip.is_bot
    fill_in "Is exploit bot", with: @ip.is_exploit_bot
    fill_in "Is hijacked", with: @ip.is_hijacked
    fill_in "Is listed", with: @ip.is_listed
    fill_in "Is malware", with: @ip.is_malware
    fill_in "Is spam bot", with: @ip.is_spam_bot
    fill_in "Is spyware", with: @ip.is_spyware
    fill_in "Last checked at", with: @ip.last_checked_at
    fill_in "Region", with: @ip.region
    click_on "Update Ip"

    assert_text "Ip was successfully updated"
    click_on "Back"
  end

  test "destroying a Ip" do
    visit ips_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ip was successfully destroyed"
  end
end
