FROM ruby:2.4.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /iprep
WORKDIR /iprep

COPY Gemfile /iprep/Gemfile
COPY Gemfile.lock /iprep/Gemfile.lock

RUN bundle install

COPY . /iprep