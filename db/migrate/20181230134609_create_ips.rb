class CreateIps < ActiveRecord::Migration[5.2]
  def change
    create_table :ips do |t|
      t.string :ip_address
      t.datetime :last_checked_at
      t.string :hostname
      t.string :city
      t.string :region
      t.string :country
      t.boolean :is_listed
      t.boolean :is_malware
      t.boolean :is_spyware
      t.boolean :is_hijacked
      t.boolean :is_bot
      t.boolean :is_spam_bot
      t.boolean :is_exploit_bot

      t.timestamps
    end
  end
end
