class CreateUserIps < ActiveRecord::Migration[5.2]
  def change
    create_table :user_ips do |t|
      t.references :user, foreign_key: true
      t.references :ip, foreign_key: true

      t.timestamps
    end
  end
end
