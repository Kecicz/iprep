json.extract! ip, :ip_address, :last_checked_at, :hostname, :country, :is_listed
json.url ip_url(ip.ip_address, format: :json)
