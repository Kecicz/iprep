class IpsController < ApplicationController
  before_action :custom_authenticate_user
  before_action :set_ip, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  # GET /ips
  # GET /ips.json
  def index
    if @user.nil?
      @user = current_user
    end 
    @ips = @user.ips
  end

  # GET /ips/1
  # GET /ips/1.json
  def show
  end

  # GET /ips/new
  def new
    @ip = Ip.new
  end

  # POST /ips
  # POST /ips.json
  def create
    if @user.nil?
      @user = current_user
    end

    @ip = Ip.find_or_initialize_by(ip_address: ip_params[:ip_address])
    @ip.last_checked_at = DateTime.now.to_s

    respond_to do |format|
      if @ip.save
        @user_ip = UserIp.find_or_initialize_by(user_id: @user.id, ip_id: @ip.id)
        @user_ip.save
        format.html { redirect_to "/ips/#{@ip.ip_address}", notice: 'Ip was successfully added to your list.' }
        format.json { render :show, status: :created, location: "/ips/#{@ip.ip_address}" }
      else
        format.html { render :new }
        format.json { render json: wrong_format_json, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ips/1
  # DELETE /ips/1.json
  def destroy
    if @user.nil?
      @user = current_user
    end

    @user_ip = UserIp.find_by(user_id: @user.id, ip_id: @ip.id)
    @user_ip.destroy

    unless UserIp.where(ip_id: @ip.id).size > 0
      @ip.destroy
    end
    respond_to do |format|
      format.html { redirect_to ips_url, notice: 'Ip was successfully removed from your list.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ip
      if @user.blank?
        @user = current_user
      end
      @ip = @user.ips.find_by(ip_address: params[:ip_address])

      if @ip.nil?
        raise ActionController::RoutingError.new('Not Found')
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ip_params
      params.require(:ip).permit(:ip_address)
    end

    def custom_authenticate_user
      puts "Trying to authenticate user"
      puts "Content type: #{request.headers['Content-type']}, API-KEY: #{request.headers['x-api-key']}"

      respond_to do |format|

        format.html do
          puts "Processing as HTML"
          authenticate_user!
        end

        format.json do
          puts "Processing as json"
          unless request.headers["x-api-key"].blank?
            @user = User.find_by(api_key: request.headers["x-api-key"])
            if @user.nil?
              render json: unauthorized_json, status: :unauthorized
            end
          else
            render json: unauthorized_json, status: :unauthorized
          end
        end
      end
    end

    def unauthorized_json
      {
        "status": 401,
        "error": "Invalid API key"
      }
    end

    def wrong_format_json
      {
        "status": 400,
        "error": "Invalid IP address format"
      }
    end
  end
