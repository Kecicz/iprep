class IpBlocklist < Flexirest::Base
  base_url "https://neutrinoapi.com/ip-blocklist"
  verbose true
  #proxy :json_api
  Flexirest::Base.faraday_config do |faraday|
    faraday.adapter(:typhoeus)
  end

  get :flex_find, "", :defaults => {:"user-id" => "kecicz", :"api-key" => "OEzH84jAM9Eiga8VgX2eHwLwuwHeyF21rfayIEmF2d3YG9zt"}

  def self.find(ip)
    self.flex_find(ip: ERB::Util.url_encode(ip))
  end
end
