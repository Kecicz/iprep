class Ip < ApplicationRecord
  has_many :user_ips
  has_many :users, :through => :user_ips

  validates :ip_address, presence: true, format: {with: /\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\b/, message: "format is invalid"}

  before_save :get_ip_info

  def get_ip_info
    ip_info = IpInfo.find(ip_address)
    ip_blocklist = IpBlocklist.find(ip_address)

    self.hostname = ip_info.hostname
    self.city = ip_info.city
    self.region = ip_info.region
    self.country = ip_info.country
    self.is_listed = ip_blocklist["is-listed"]
    self.is_malware = ip_blocklist["is-malware"]
    self.is_spyware = ip_blocklist["is-spyware"]
    self.is_hijacked = ip_blocklist["is-hijacked"]
    self.is_bot = ip_blocklist["is-bot"]
    self.is_spam_bot = ip_blocklist["is-spam-bot"]
    self.is_exploit_bot = ip_blocklist["is-exploit-bot"]
  end

end
