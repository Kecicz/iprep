class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :user_ips
  has_many :ips, :through => :user_ips

  before_create :generate_api_key

  def generate_api_key
    self.api_key = Base64.urlsafe_encode64(PBKDF2.new(password: encrypted_password, salt: email, iterations: 1000).value)
  end

end
