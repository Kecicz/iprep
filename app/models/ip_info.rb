class IpInfo < Flexirest::Base
  base_url "https://ipinfo.io/"
  verbose true
  #proxy :json_api
  Flexirest::Base.faraday_config do |faraday|
    faraday.adapter(:typhoeus)
  end

  get :find, "/:id", :defaults => {:token => "59314d31bb1085"}
end
